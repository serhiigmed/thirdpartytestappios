//
//  ViewController.swift
//  TestApp
//
//  Created by Serhii Biloshkurskyi on 10/23/20.
//

import UIKit

class ViewController: UIViewController {
    
    var token: String? {
        didSet {
            moveToPrizmaButton.isEnabled = token?.isEmpty == false ? true : false
        }
    }
    
    @IBOutlet weak var loginTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var moveToPrizmaButton: UIButton!
    @IBOutlet weak var segmetControl: UISegmentedControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        loginTextField.text = "test-datos@mail.com"
//        passwordTextField.text = "Qwerty123"
        moveToPrizmaButton.isEnabled = false
    }
    @IBAction func onLoginButton(_ sender: Any) {
        let url = URL(string: "https://prizmaqace.azurewebsites.net/user/login")!
        var request = URLRequest(url: url)
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("1", forHTTPHeaderField: "platform")
        request.setValue("255.1.1.1", forHTTPHeaderField: "sw_ver")
        request.httpMethod = "POST"
        let parameters = ["email": loginTextField.text,
                          "password": passwordTextField.text]
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
        } catch let error {
            print(error.localizedDescription)
        }
        
        let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            guard error == nil else { return }
            guard let data = data else { return }
            
            do {
                //create json object from data
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                    print(json)
                    DispatchQueue.main.async { [weak self] in
                        self?.token = json["token"] as? String
                    }
                }
            } catch let error {
                print(error.localizedDescription)
            }
        })
        task.resume()
    }
    
    @IBAction func onMiveToPrizmaButton(_ sender: Any) {
        guard let token = token, let userId = loginTextField.text else { return }
        
        //testapp
        let url = URL(string:"prizmaapp://project_id=foobar12&user_id=\(userId)&token=\(token)&m_type=\(segmetControl.selectedSegmentIndex)&app_url=testapp&title=Test%20app")
        
        UIApplication.shared.open(url!) { (result) in
            if result {
                print("r: \(result)")
               // The URL was delivered successfully!
            }
        }
    }
}
